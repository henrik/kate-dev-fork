# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Chetan Khona <chetan@kompkin.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-26 01:47+0000\n"
"PO-Revision-Date: 2013-02-26 11:54+0530\n"
"Last-Translator: Chetan Khona <chetan@kompkin.com>\n"
"Language-Team: Marathi <kde-i18n-doc@kde.org>\n"
"Language: mr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 1.5\n"

#: branchcheckoutdialog.cpp:29
#, kde-format
msgid "Select branch to checkout. Press 'Esc' to cancel."
msgstr ""

#: branchcheckoutdialog.cpp:36
#, kde-format
msgid "Create New Branch"
msgstr ""

#: branchcheckoutdialog.cpp:38
#, kde-format
msgid "Create New Branch From..."
msgstr ""

#: branchcheckoutdialog.cpp:53
#, kde-format
msgid "Branch %1 checked out"
msgstr ""

#: branchcheckoutdialog.cpp:56
#, kde-format
msgid "Failed to checkout to branch %1, Error: %2"
msgstr ""

#: branchcheckoutdialog.cpp:83 branchcheckoutdialog.cpp:95
#, kde-format
msgid "Enter new branch name. Press 'Esc' to cancel."
msgstr ""

#: branchcheckoutdialog.cpp:100
#, kde-format
msgid "Select branch to checkout from. Press 'Esc' to cancel."
msgstr ""

#: branchcheckoutdialog.cpp:121
#, kde-format
msgid "Checked out to new branch: %1"
msgstr ""

#: branchcheckoutdialog.cpp:123
#, kde-format
msgid "Failed to create new branch. Error \"%1\""
msgstr ""

#: branchdeletedialog.cpp:126
#, kde-format
msgid "Branch"
msgstr ""

#: branchdeletedialog.cpp:126
#, kde-format
msgid "Last Commit"
msgstr ""

#: branchdeletedialog.cpp:139 kateprojecttreeviewcontextmenu.cpp:78
#, kde-format
msgid "Delete"
msgstr ""

#: branchdeletedialog.cpp:144
#, kde-format
msgid "Are you sure you want to delete the selected branch?"
msgid_plural "Are you sure you want to delete the selected branches?"
msgstr[0] ""
msgstr[1] ""

#: branchesdialog.cpp:100
#, kde-format
msgid "Select Branch..."
msgstr ""

#: branchesdialog.cpp:126 gitwidget.cpp:534 kateprojectpluginview.cpp:67
#, kde-format
msgid "Git"
msgstr ""

#: comparebranchesview.cpp:145
#, kde-format
msgid "Back"
msgstr ""

#: currentgitbranchbutton.cpp:124
#, kde-format
msgctxt "Tooltip text, describing that '%1' commit is checked out"
msgid "HEAD at commit %1"
msgstr ""

#: currentgitbranchbutton.cpp:126
#, kde-format
msgctxt "Tooltip text, describing that '%1' tag is checked out"
msgid "HEAD is at this tag %1"
msgstr ""

#: currentgitbranchbutton.cpp:128
#, kde-format
msgctxt "Tooltip text, describing that '%1' branch is checked out"
msgid "Active branch: %1"
msgstr ""

#: gitcommitdialog.cpp:69 gitcommitdialog.cpp:116
#, kde-format
msgid "Commit Changes"
msgstr ""

#: gitcommitdialog.cpp:73 gitcommitdialog.cpp:115 gitwidget.cpp:262
#, kde-format
msgid "Commit"
msgstr ""

#: gitcommitdialog.cpp:74
#, kde-format
msgid "Cancel"
msgstr ""

#: gitcommitdialog.cpp:76
#, kde-format
msgid "Write commit message..."
msgstr ""

#: gitcommitdialog.cpp:83
#, kde-format
msgid "Extended commit description..."
msgstr ""

#: gitcommitdialog.cpp:107
#, kde-format
msgid "Sign off"
msgstr ""

#: gitcommitdialog.cpp:111 gitcommitdialog.cpp:120
#, kde-format
msgid "Amend"
msgstr ""

#: gitcommitdialog.cpp:112 gitwidget.cpp:1005
#, kde-format
msgid "Amend Last Commit"
msgstr ""

#: gitcommitdialog.cpp:119
#, kde-format
msgid "Amending Commit"
msgstr ""

#: gitcommitdialog.cpp:203
#, kde-format
msgctxt "Number of characters"
msgid "%1 / 52"
msgstr ""

#: gitcommitdialog.cpp:207
#, kde-format
msgctxt "Number of characters"
msgid "<span style=\"color:%1;\">%2</span> / 52"
msgstr ""

#: gitstatusmodel.cpp:85
#, kde-format
msgid "Staged"
msgstr ""

#: gitstatusmodel.cpp:87
#, kde-format
msgid "Untracked"
msgstr ""

#: gitstatusmodel.cpp:89
#, kde-format
msgid "Conflict"
msgstr ""

#: gitstatusmodel.cpp:91
#, kde-format
msgid "Modified"
msgstr ""

#: gitwidget.cpp:270
#, kde-format
msgid "Git Push"
msgstr ""

#: gitwidget.cpp:283
#, kde-format
msgid "Git Pull"
msgstr ""

#: gitwidget.cpp:296
#, kde-format
msgid "Cancel Operation"
msgstr ""

#: gitwidget.cpp:304
#, kde-format
msgid " canceled."
msgstr ""

#: gitwidget.cpp:325 kateprojectview.cpp:59
#, kde-format
msgid "Filter..."
msgstr ""

#: gitwidget.cpp:413
#, kde-format
msgid "Failed to find .git directory for '%1', things may not work correctly"
msgstr ""

#: gitwidget.cpp:618
#, kde-format
msgid " error: %1"
msgstr ""

#: gitwidget.cpp:624
#, kde-format
msgid "\"%1\" executed successfully: %2"
msgstr ""

#: gitwidget.cpp:644
#, kde-format
msgid "Failed to stage file. Error:"
msgstr ""

#: gitwidget.cpp:657
#, kde-format
msgid "Failed to unstage file. Error:"
msgstr ""

#: gitwidget.cpp:668
#, kde-format
msgid "Failed to discard changes. Error:"
msgstr ""

#: gitwidget.cpp:679
#, kde-format
msgid "Failed to remove. Error:"
msgstr ""

#: gitwidget.cpp:695
#, kde-format
msgid "Failed to open file at HEAD: %1"
msgstr ""

#: gitwidget.cpp:727
#, kde-format
msgid "Failed to get Diff of file: %1"
msgstr ""

#: gitwidget.cpp:794
#, kde-format
msgid "Failed to commit: %1"
msgstr ""

#: gitwidget.cpp:798
#, kde-format
msgid "Changes committed successfully."
msgstr ""

#: gitwidget.cpp:808
#, kde-format
msgid "Nothing to commit. Please stage your changes first."
msgstr ""

#: gitwidget.cpp:821
#, kde-format
msgid "Commit message cannot be empty."
msgstr ""

#: gitwidget.cpp:937
#, kde-format
msgid "No diff for %1...%2"
msgstr ""

#: gitwidget.cpp:943
#, kde-format
msgid "Failed to compare %1...%2"
msgstr ""

#: gitwidget.cpp:958
#, kde-format
msgid "Failed to get numstat when diffing %1...%2"
msgstr ""

#: gitwidget.cpp:997
#, kde-format
msgid "Refresh"
msgstr ""

#: gitwidget.cpp:1013
#, kde-format
msgid "Checkout Branch"
msgstr ""

#: gitwidget.cpp:1025
#, kde-format
msgid "Delete Branch"
msgstr ""

#: gitwidget.cpp:1037
#, kde-format
msgid "Compare Branch with..."
msgstr ""

#: gitwidget.cpp:1042
#, kde-format
msgid "Show Commit"
msgstr ""

#: gitwidget.cpp:1042
#, kde-format
msgid "Commit hash"
msgstr ""

#: gitwidget.cpp:1049
#, fuzzy, kde-format
#| msgid "Open With"
msgid "Open Commit..."
msgstr "यामध्ये उघडा"

#: gitwidget.cpp:1052 gitwidget.cpp:1091
#, kde-format
msgid "Stash"
msgstr ""

#: gitwidget.cpp:1062
#, kde-format
msgid "Diff - stash"
msgstr ""

#: gitwidget.cpp:1095
#, kde-format
msgid "Pop Last Stash"
msgstr ""

#: gitwidget.cpp:1099
#, kde-format
msgid "Pop Stash"
msgstr ""

#: gitwidget.cpp:1103
#, kde-format
msgid "Apply Last Stash"
msgstr ""

#: gitwidget.cpp:1105
#, kde-format
msgid "Stash (Keep Staged)"
msgstr ""

#: gitwidget.cpp:1109
#, kde-format
msgid "Stash (Include Untracked)"
msgstr ""

#: gitwidget.cpp:1113
#, kde-format
msgid "Apply Stash"
msgstr ""

#: gitwidget.cpp:1114
#, kde-format
msgid "Drop Stash"
msgstr ""

#: gitwidget.cpp:1115
#, kde-format
msgid "Show Stash Content"
msgstr ""

#: gitwidget.cpp:1151
#, kde-format
msgid "Stage All"
msgstr ""

#: gitwidget.cpp:1153
#, kde-format
msgid "Remove All"
msgstr ""

#: gitwidget.cpp:1153
#, kde-format
msgid "Discard All"
msgstr ""

#: gitwidget.cpp:1156
#, kde-format
msgid "Open .gitignore"
msgstr ""

#: gitwidget.cpp:1157 gitwidget.cpp:1204 gitwidget.cpp:1246
#: kateprojectconfigpage.cpp:89 kateprojectconfigpage.cpp:101
#, kde-format
msgid "Show Diff"
msgstr ""

#: gitwidget.cpp:1174
#, kde-format
msgid "Are you sure you want to remove these files?"
msgstr ""

#: gitwidget.cpp:1179
#, kde-format
msgid "Are you sure you want to discard all changes?"
msgstr ""

#: gitwidget.cpp:1203
#, fuzzy, kde-format
#| msgid "Open With"
msgid "Open File"
msgstr "यामध्ये उघडा"

#: gitwidget.cpp:1205
#, kde-format
msgid "Show in External Git Diff Tool"
msgstr ""

#: gitwidget.cpp:1206
#, kde-format
msgid "Open at HEAD"
msgstr ""

#: gitwidget.cpp:1207
#, kde-format
msgid "Unstage File"
msgstr ""

#: gitwidget.cpp:1207
#, kde-format
msgid "Stage File"
msgstr ""

#: gitwidget.cpp:1208
#, kde-format
msgid "Remove"
msgstr ""

#: gitwidget.cpp:1208
#, kde-format
msgid "Discard"
msgstr ""

#: gitwidget.cpp:1225
#, kde-format
msgid "Are you sure you want to discard the changes in this file?"
msgstr ""

#: gitwidget.cpp:1234
#, kde-format
msgid "Are you sure you want to remove this file?"
msgstr ""

#: gitwidget.cpp:1245
#, kde-format
msgid "Unstage All"
msgstr ""

#: gitwidget.cpp:1312
#, kde-format
msgid "Unstage Selected Files"
msgstr ""

#: gitwidget.cpp:1312
#, kde-format
msgid "Stage Selected Files"
msgstr ""

#: gitwidget.cpp:1313
#, kde-format
msgid "Discard Selected Files"
msgstr ""

#: gitwidget.cpp:1317
#, kde-format
msgid "Remove Selected Files"
msgstr ""

#: gitwidget.cpp:1330
#, kde-format
msgid "Are you sure you want to discard the changes?"
msgstr ""

#: gitwidget.cpp:1335
#, kde-format
msgid "Are you sure you want to remove these untracked changes?"
msgstr ""

#: kateproject.cpp:218
#, kde-format
msgid "Malformed JSON file '%1': %2"
msgstr ""

#: kateproject.cpp:518
#, kde-format
msgid "<untracked>"
msgstr ""

#: kateprojectcompletion.cpp:54
#, kde-format
msgid "Project Completion"
msgstr "परियोजना पूर्णत्व"

#: kateprojectconfigpage.cpp:25
#, kde-format
msgctxt "Groupbox title"
msgid "Autoload Repositories"
msgstr ""

#: kateprojectconfigpage.cpp:27
#, kde-format
msgid ""
"Project plugin is able to autoload repository working copies when there is "
"no .kateproject file defined yet."
msgstr ""

#: kateprojectconfigpage.cpp:30
#, kde-format
msgid "&Git"
msgstr ""

#: kateprojectconfigpage.cpp:33
#, kde-format
msgid "&Subversion"
msgstr ""

#: kateprojectconfigpage.cpp:35
#, kde-format
msgid "&Mercurial"
msgstr ""

#: kateprojectconfigpage.cpp:37
#, kde-format
msgid "&Fossil"
msgstr ""

#: kateprojectconfigpage.cpp:46
#, kde-format
msgctxt "Groupbox title"
msgid "Session Behavior"
msgstr ""

#: kateprojectconfigpage.cpp:47
#, kde-format
msgid "Session settings for projects"
msgstr ""

#: kateprojectconfigpage.cpp:48
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Restore Open Projects"
msgstr "वर्तमान परियोजना"

#: kateprojectconfigpage.cpp:55
#, fuzzy, kde-format
#| msgid "Projects"
msgctxt "Groupbox title"
msgid "Project Index"
msgstr "परियोजना"

#: kateprojectconfigpage.cpp:56
#, kde-format
msgid "Project ctags index settings"
msgstr ""

#: kateprojectconfigpage.cpp:57 kateprojectinfoviewindex.cpp:208
#, kde-format
msgid "Enable indexing"
msgstr ""

#: kateprojectconfigpage.cpp:60
#, kde-format
msgid "Directory for index files"
msgstr ""

#: kateprojectconfigpage.cpp:64
#, kde-format
msgid ""
"The system temporary directory is used if not specified, which may overflow "
"for very large repositories"
msgstr ""

#: kateprojectconfigpage.cpp:71
#, kde-format
msgctxt "Groupbox title"
msgid "Cross-Project Functionality"
msgstr ""

#: kateprojectconfigpage.cpp:72
#, kde-format
msgid ""
"Project plugin is able to perform some operations across multiple projects"
msgstr ""

#: kateprojectconfigpage.cpp:73
#, fuzzy, kde-format
#| msgid "Project Completion"
msgid "Cross-Project Completion"
msgstr "परियोजना पूर्णत्व"

#: kateprojectconfigpage.cpp:75
#, kde-format
msgid "Cross-Project Goto Symbol"
msgstr ""

#: kateprojectconfigpage.cpp:83
#, kde-format
msgctxt "Groupbox title"
msgid "Git"
msgstr ""

#: kateprojectconfigpage.cpp:86
#, kde-format
msgid "Single click action in the git status view"
msgstr ""

#: kateprojectconfigpage.cpp:88 kateprojectconfigpage.cpp:100
#, kde-format
msgid "No Action"
msgstr ""

#: kateprojectconfigpage.cpp:90 kateprojectconfigpage.cpp:102
#, fuzzy, kde-format
#| msgid "Open With"
msgid "Open file"
msgstr "यामध्ये उघडा"

#: kateprojectconfigpage.cpp:91 kateprojectconfigpage.cpp:103
#, kde-format
msgid "Stage / Unstage"
msgstr ""

#: kateprojectconfigpage.cpp:98
#, kde-format
msgid "Double click action in the git status view"
msgstr ""

#: kateprojectconfigpage.cpp:134 kateprojectpluginview.cpp:66
#, kde-format
msgid "Projects"
msgstr "परियोजना"

#: kateprojectconfigpage.cpp:139
#, fuzzy, kde-format
#| msgid "Project Completion"
msgctxt "Groupbox title"
msgid "Projects Properties"
msgstr "परियोजना पूर्णत्व"

#: kateprojectinfoview.cpp:34
#, kde-format
msgid "Terminal (.kateproject)"
msgstr ""

#: kateprojectinfoview.cpp:42
#, fuzzy, kde-format
#| msgid "Terminal"
msgid "Terminal (Base)"
msgstr "टर्मिनल"

#: kateprojectinfoview.cpp:49
#, kde-format
msgid "Code Index"
msgstr "कोड अनुक्रमणिका"

#: kateprojectinfoview.cpp:54
#, kde-format
msgid "Code Analysis"
msgstr "कोड विश्लेषण"

#: kateprojectinfoview.cpp:59
#, kde-format
msgid "Notes"
msgstr "नोंदी"

#: kateprojectinfoviewcodeanalysis.cpp:34
#, kde-format
msgid "Start Analysis..."
msgstr "विश्लेषण सुरु करा..."

#: kateprojectinfoviewcodeanalysis.cpp:41
#, fuzzy, kde-format
#| msgid "Code Analysis"
msgctxt "'%1' refers to project name, e.g,. Code Analysis - MyProject"
msgid "Code Analysis - %1"
msgstr "कोड विश्लेषण"

#: kateprojectinfoviewcodeanalysis.cpp:100
#, kde-format
msgid ""
"%1<br/><br/>The tool will be run on all project files which match this list "
"of file extensions:<br/><br/><b>%2</b>"
msgstr ""

#: kateprojectinfoviewcodeanalysis.cpp:138
#: kateprojectinfoviewcodeanalysis.cpp:198
#: kateprojectinfoviewcodeanalysis.cpp:202
#, fuzzy, kde-format
#| msgid "Code Analysis"
msgid "CodeAnalysis"
msgstr "कोड विश्लेषण"

#: kateprojectinfoviewcodeanalysis.cpp:193
#, kde-format
msgctxt ""
"Message to the user that analysis finished. %1 is the name of the program "
"that did the analysis, %2 is a number. e.g., [clang-tidy]Analysis on 5 files "
"finished"
msgid "[%1]Analysis on %2 file finished."
msgid_plural "[%1]Analysis on %2 files finished."
msgstr[0] ""
msgstr[1] ""

#: kateprojectinfoviewcodeanalysis.cpp:201
#, kde-format
msgid "Analysis failed with exit code %1, Error: %2"
msgstr ""

#: kateprojectinfoviewindex.cpp:35
#, kde-format
msgid "Name"
msgstr ""

#: kateprojectinfoviewindex.cpp:35
#, kde-format
msgid "Kind"
msgstr ""

#: kateprojectinfoviewindex.cpp:35
#, kde-format
msgid "File"
msgstr ""

#: kateprojectinfoviewindex.cpp:35
#, kde-format
msgid "Line"
msgstr ""

#: kateprojectinfoviewindex.cpp:36
#, kde-format
msgid "Search"
msgstr ""

#: kateprojectinfoviewindex.cpp:197
#, kde-format
msgid "The index could not be created. Please install 'ctags'."
msgstr "अनुक्रमणिका तयार करू शकत नाही. कृपया 'ctags' प्रतिष्ठापीत करा."

#: kateprojectinfoviewindex.cpp:207
#, kde-format
msgid "Indexing is not enabled"
msgstr ""

#: kateprojectitem.cpp:166
#, kde-format
msgid "Error"
msgstr ""

#: kateprojectitem.cpp:166
#, kde-format
msgid "File name already exists"
msgstr ""

#: kateprojectplugin.cpp:217
#, kde-format
msgid "Confirm project closing: %1"
msgstr ""

#: kateprojectplugin.cpp:218
#, kde-format
msgid "Do you want to close the project %1 and the related %2 open documents?"
msgstr ""

#: kateprojectplugin.cpp:557
#, kde-format
msgid "Full path to current project excluding the file name."
msgstr ""

#: kateprojectplugin.cpp:574
#, kde-format
msgid ""
"Full path to current project excluding the file name, with native path "
"separator (backslash on Windows)."
msgstr ""

#: kateprojectplugin.cpp:691 kateprojectpluginview.cpp:72
#: kateprojectpluginview.cpp:228 kateprojectviewtree.cpp:136
#: kateprojectviewtree.cpp:157
#, fuzzy, kde-format
#| msgid "Projects"
msgid "Project"
msgstr "परियोजना"

#: kateprojectpluginview.cpp:56
#, fuzzy, kde-format
#| msgid "Projects"
msgid "Project Manager"
msgstr "परियोजना"

#: kateprojectpluginview.cpp:78
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Open projects list"
msgstr "वर्तमान परियोजना"

#: kateprojectpluginview.cpp:83
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Reload project"
msgstr "वर्तमान परियोजना"

#: kateprojectpluginview.cpp:86
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Close project"
msgstr "वर्तमान परियोजना"

#: kateprojectpluginview.cpp:101
#, kde-format
msgid "Refresh git status"
msgstr ""

#: kateprojectpluginview.cpp:179
#, fuzzy, kde-format
#| msgid "Open With"
msgid "Open Folder..."
msgstr "यामध्ये उघडा"

#: kateprojectpluginview.cpp:184
#, fuzzy, kde-format
#| msgid "Projects"
msgid "Project TODOs"
msgstr "परियोजना"

#: kateprojectpluginview.cpp:188
#, kde-format
msgid "Activate Previous Project"
msgstr ""

#: kateprojectpluginview.cpp:193
#, kde-format
msgid "Activate Next Project"
msgstr ""

#: kateprojectpluginview.cpp:198
#, kde-format
msgid "Lookup"
msgstr ""

#: kateprojectpluginview.cpp:202
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Close Project"
msgstr "वर्तमान परियोजना"

#: kateprojectpluginview.cpp:206
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Close All Projects"
msgstr "वर्तमान परियोजना"

#: kateprojectpluginview.cpp:211
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Close Orphaned Projects"
msgstr "वर्तमान परियोजना"

#: kateprojectpluginview.cpp:215
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Reload Project"
msgstr "वर्तमान परियोजना"

#: kateprojectpluginview.cpp:225
#, kde-format
msgid "Checkout Git Branch"
msgstr ""

#: kateprojectpluginview.cpp:231 kateprojectpluginview.cpp:805
#, kde-format
msgid "Lookup: %1"
msgstr ""

#: kateprojectpluginview.cpp:232 kateprojectpluginview.cpp:806
#, kde-format
msgid "Goto: %1"
msgstr ""

#: kateprojectpluginview.cpp:306
#, fuzzy, kde-format
#| msgid "Projects"
msgid "Projects Index"
msgstr "परियोजना"

#: kateprojectpluginview.cpp:850
#, kde-format
msgid "Choose a directory"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:42
#, kde-format
msgid "Enter name:"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:43
#, kde-format
msgid "Add"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:64
#, kde-format
msgid "Copy Location"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:69
#, kde-format
msgid "Add File"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:70
#, kde-format
msgid "Add Folder"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:77
#, kde-format
msgid "&Rename"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:84
#, fuzzy, kde-format
#| msgid "Project Completion"
msgid "Properties"
msgstr "परियोजना पूर्णत्व"

#: kateprojecttreeviewcontextmenu.cpp:88
#, kde-format
msgid "Open With"
msgstr "यामध्ये उघडा"

#: kateprojecttreeviewcontextmenu.cpp:96
#, kde-format
msgid "Open Internal Terminal Here"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:105
#, kde-format
msgid "Open External Terminal Here"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:110
#, kde-format
msgid "&Open Containing Folder"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:119
#, kde-format
msgid "Show Git History"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:124
#, kde-format
msgid "Delete File"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:125
#, kde-format
msgid "Do you want to delete the file '%1'?"
msgstr ""

#: kateprojectviewtree.cpp:136
#, kde-format
msgid "Failed to create file: %1, Error: %2"
msgstr ""

#: kateprojectviewtree.cpp:157
#, kde-format
msgid "Failed to create dir: %1"
msgstr ""

#: stashdialog.cpp:36
#, kde-format
msgid "Stash message (optional). Enter to confirm, Esc to leave."
msgstr ""

#: stashdialog.cpp:43
#, kde-format
msgid "Type to filter, Enter to pop stash, Esc to leave."
msgstr ""

#: stashdialog.cpp:142
#, kde-format
msgid "Failed to stash changes %1"
msgstr ""

#: stashdialog.cpp:144
#, kde-format
msgid "Changes stashed successfully."
msgstr ""

#: stashdialog.cpp:163
#, kde-format
msgid "Failed to get stash list. Error: "
msgstr ""

#: stashdialog.cpp:179
#, kde-format
msgid "Failed to apply stash. Error: "
msgstr ""

#: stashdialog.cpp:181
#, kde-format
msgid "Failed to drop stash. Error: "
msgstr ""

#: stashdialog.cpp:183
#, kde-format
msgid "Failed to pop stash. Error: "
msgstr ""

#: stashdialog.cpp:187
#, kde-format
msgid "Stash applied successfully."
msgstr ""

#: stashdialog.cpp:189
#, kde-format
msgid "Stash dropped successfully."
msgstr ""

#: stashdialog.cpp:191
#, kde-format
msgid "Stash popped successfully."
msgstr ""

#: stashdialog.cpp:220
#, kde-format
msgid "Show stash failed. Error: "
msgstr ""

#. i18n: ectx: Menu (projects)
#: ui.rc:9
#, kde-format
msgid "&Projects"
msgstr "परियोजना (&P)"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "चेतन खोना"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "chetan@kompkin.com"

#, fuzzy
#~| msgid "Open With"
#~ msgid "Type to filter..."
#~ msgstr "यामध्ये उघडा"

#~ msgid "Hello World"
#~ msgstr "हेलो वर्ल्ड"

#~ msgid "Example kate plugin"
#~ msgstr "उदाहरण केट प्लगइन"
