# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kate package.
#
# Kheyyam Gojayev <xxmn77@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-19 01:01+0000\n"
"PO-Revision-Date: 2022-05-05 20:10+0400\n"
"Last-Translator: Kheyyam Gojayev <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.04.0\n"

#: tabswitcher.cpp:49
#, kde-format
msgid "Document Switcher"
msgstr "Sənəd dəyişdricisi"

#: tabswitcher.cpp:94
#, kde-format
msgid "Last Used Views"
msgstr "Son istifadə olunan görüntülər"

#: tabswitcher.cpp:97
#, kde-format
msgid "Opens a list to walk through the list of last used views."
msgstr "Son istifadə olunanan görüntülərə baxmaq üçün siyahını açın"

#: tabswitcher.cpp:98 tabswitcher.cpp:106
#, kde-format
msgid "Walk through the list of last used views"
msgstr "Son istifadə olunan görüntülərin siyahısına baxmaq"

#: tabswitcher.cpp:102
#, kde-format
msgid "Last Used Views (Reverse)"
msgstr "Son istifadə olunan görüntülər (əks tərəfə)"

#: tabswitcher.cpp:105
#, kde-format
msgid "Opens a list to walk through the list of last used views in reverse."
msgstr ""
"Son istifadə olunanan görüntülər siyahısına əksinə baxmaq üçün siyahını açın."

#: tabswitcher.cpp:110
#, kde-format
msgid "Close View"
msgstr "Görüntünü bağlayın"

#: tabswitcher.cpp:113 tabswitcher.cpp:114
#, kde-format
msgid "Closes the selected view in the list of last used views."
msgstr ""
"Son istifadə olunan görüntülərin siyahısında seçilmiş görüntünü bağlayın."

#. i18n: ectx: Menu (go)
#: ui.rc:6
#, kde-format
msgid "&Go"
msgstr "&İrəli"

#~ msgid "&View"
#~ msgstr "&Görüntü"
