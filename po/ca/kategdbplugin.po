# Translation of kategdbplugin.po to Catalan
# Copyright (C) 2010-2023 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Manuel Tortosa Moreno <manutortosa@chakra-project.org>, 2010.
# Josep M. Ferrer <txemaq@gmail.com>, 2011, 2012, 2013, 2014, 2015, 2020, 2022, 2023.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2012, 2013, 2014, 2015, 2016.
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-15 01:54+0000\n"
"PO-Revision-Date: 2023-09-19 19:23+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"

#. i18n: ectx: property (text), widget (QLabel, u_gdbLabel)
#: advanced_settings.ui:17
#, kde-format
msgid "GDB command"
msgstr "Ordre del GDB"

#. i18n: ectx: property (text), widget (QToolButton, u_gdbBrowse)
#. i18n: ectx: property (text), widget (QToolButton, u_addSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_setSoPrefix)
#. i18n: ectx: property (text), widget (QToolButton, u_addSoSearchPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSoSearchPath)
#: advanced_settings.ui:30 advanced_settings.ui:62 advanced_settings.ui:69
#: advanced_settings.ui:241 advanced_settings.ui:274 advanced_settings.ui:281
#, kde-format
msgid "..."
msgstr "..."

#. i18n: ectx: property (text), widget (QLabel, u_srcPathsLabel)
#: advanced_settings.ui:37
#, kde-format
msgid "Source file search paths"
msgstr "Camins de cerca de fitxers de codi font"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:92
#, kde-format
msgid "Local application"
msgstr "Aplicació local"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:97
#, kde-format
msgid "Remote TCP"
msgstr "TCP remot"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:102
#, kde-format
msgid "Remote Serial Port"
msgstr "Port sèrie remot"

#. i18n: ectx: property (text), widget (QLabel, u_hostLabel)
#: advanced_settings.ui:127
#, kde-format
msgid "Host"
msgstr "Màquina"

#. i18n: ectx: property (text), widget (QLabel, u_tcpPortLabel)
#. i18n: ectx: property (text), widget (QLabel, u_ttyLabel)
#: advanced_settings.ui:141 advanced_settings.ui:166
#, kde-format
msgid "Port"
msgstr "Port"

#. i18n: ectx: property (text), widget (QLabel, u_ttyBaudLabel)
#: advanced_settings.ui:183
#, kde-format
msgid "Baud"
msgstr "Baud"

#. i18n: ectx: property (text), widget (QLabel, u_soAbsPrefixLabel)
#: advanced_settings.ui:231
#, kde-format
msgid "solib-absolute-prefix"
msgstr "solib-absolute-prefix"

#. i18n: ectx: property (text), widget (QLabel, u_soSearchLabel)
#: advanced_settings.ui:248
#, kde-format
msgid "solib-search-path"
msgstr "solib-search-path"

#. i18n: ectx: property (title), widget (QGroupBox, u_customInitGB)
#: advanced_settings.ui:317
#, kde-format
msgid "Custom Init Commands"
msgstr "Ordres d'inici personalitzades"

#: backend.cpp:24 backend.cpp:49 debugview_dap.cpp:155
#, kde-format
msgid ""
"A debugging session is on course. Please, use re-run or stop the current "
"session."
msgstr ""
"Hi ha una sessió de depuració en curs. Torneu a executar o atureu la sessió "
"actual."

#: configview.cpp:92
#, kde-format
msgid "Add new target"
msgstr "Afegeix un objectiu nou"

#: configview.cpp:96
#, kde-format
msgid "Copy target"
msgstr "Copia un objectiu"

#: configview.cpp:100
#, kde-format
msgid "Delete target"
msgstr "Elimina un objectiu"

#: configview.cpp:105
#, kde-format
msgid "Executable:"
msgstr "Executable:"

#: configview.cpp:125
#, kde-format
msgid "Working Directory:"
msgstr "Directori de treball:"

#: configview.cpp:133
#, kde-format
msgid "Process Id:"
msgstr "ID del procés:"

#: configview.cpp:138
#, kde-format
msgctxt "Program argument list"
msgid "Arguments:"
msgstr "Arguments:"

#: configview.cpp:141
#, kde-format
msgctxt "Checkbox to for keeping focus on the command line"
msgid "Keep focus"
msgstr "Mantén el focus"

#: configview.cpp:142
#, kde-format
msgid "Keep the focus on the command line"
msgstr "Mantén el focus a la línia d'ordres"

#: configview.cpp:144
#, kde-format
msgid "Redirect IO"
msgstr "Redirigeix la E/S"

#: configview.cpp:145
#, kde-format
msgid "Redirect the debugged programs IO to a separate tab"
msgstr "Redirigeix la E/S dels programes depurats a una pestanya separada"

#: configview.cpp:147
#, kde-format
msgid "Advanced Settings"
msgstr "Configuració avançada"

#: configview.cpp:231
#, kde-format
msgid "Targets"
msgstr "Objectius"

#: configview.cpp:524 configview.cpp:537
#, kde-format
msgid "Target %1"
msgstr "Objectiu %1"

#. i18n: ectx: attribute (title), widget (QWidget, tab_1)
#: debugconfig.ui:33
#, kde-format
msgid "User Debug Adapter Settings"
msgstr "Paràmetres d'usuari de l'adaptador de depuració"

#. i18n: ectx: property (text), widget (QLabel, label)
#: debugconfig.ui:41
#, kde-format
msgid "Settings File:"
msgstr "Fitxer de configuració:"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: debugconfig.ui:68
#, kde-format
msgid "Default Debug Adapter Settings"
msgstr "Paràmetres predeterminats de l'adaptador de depuració"

#: debugconfigpage.cpp:72 debugconfigpage.cpp:77
#, kde-format
msgid "Debugger"
msgstr "Depurador"

#: debugconfigpage.cpp:128
#, kde-format
msgid "No JSON data to validate."
msgstr "No hi ha dades JSON a validar."

#: debugconfigpage.cpp:136
#, kde-format
msgid "JSON data is valid."
msgstr "Les dades JSON són vàlides."

#: debugconfigpage.cpp:138
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr "Les dades JSON no són vàlides: no és un objecte JSON"

#: debugconfigpage.cpp:141
#, kde-format
msgid "JSON data is invalid: %1"
msgstr "Les dades JSON no són vàlides: %1"

#: debugview.cpp:35
#, kde-format
msgid "Locals"
msgstr "Locals"

#: debugview.cpp:37
#, kde-format
msgid "CPU registers"
msgstr "Registres de la CPU"

#: debugview.cpp:160
#, kde-format
msgid "Please set the executable in the 'Settings' tab in the 'Debug' panel."
msgstr ""
"Establiu l'executable a la pestanya «Configuració» del plafó «Depuració»."

#: debugview.cpp:169
#, kde-format
msgid ""
"No debugger selected. Please select one in the 'Settings' tab in the 'Debug' "
"panel."
msgstr ""
"No hi ha cap depurador seleccionat. Establiu l'executable a la pestanya "
"«Configuració» del plafó «Depuració»."

#: debugview.cpp:178
#, kde-format
msgid ""
"Debugger not found. Please make sure you have it installed in your system. "
"The selected debugger is '%1'"
msgstr ""
"No s'ha trobat cap depurador. Assegureu-vos que està instal·lat en el "
"sistema. El depurador seleccionat és «%1»"

#: debugview.cpp:384
#, kde-format
msgid "Could not start debugger process"
msgstr "No s'ha pogut iniciar el procés de depuració"

#: debugview.cpp:442
#, kde-format
msgid "*** gdb exited normally ***"
msgstr "*** El «gdb» ha finalitzat amb normalitat ***"

#: debugview.cpp:648
#, kde-format
msgid "all threads running"
msgstr "tots els fils en execució"

#: debugview.cpp:650
#, kde-format
msgid "thread(s) running: %1"
msgstr "fil/s en execució: %1"

#: debugview.cpp:655 debugview_dap.cpp:270
#, kde-format
msgid "stopped (%1)."
msgstr "aturat (%1)."

#: debugview.cpp:659 debugview_dap.cpp:278
#, kde-format
msgid "Active thread: %1 (all threads stopped)."
msgstr "Fil actiu: %1 (tots els fils aturats)."

#: debugview.cpp:661 debugview_dap.cpp:280
#, kde-format
msgid "Active thread: %1."
msgstr "Fil actiu: %1."

#: debugview.cpp:680
#, kde-format
msgid "Current frame: %1:%2"
msgstr "Marc actual: %1:%2"

#: debugview.cpp:707
#, kde-format
msgid "Host: %1. Target: %1"
msgstr "Màquina: %1. Objectiu: %1"

#: debugview.cpp:1377
#, kde-format
msgid ""
"gdb-mi: Could not parse last response: %1. Too many consecutive errors. "
"Shutting down."
msgstr ""
"gdb-mi: no s'ha pogut analitzar la darrera resposta: %1. Massa errors "
"consecutius. S'aturarà."

#: debugview.cpp:1379
#, kde-format
msgid "gdb-mi: Could not parse last response: %1"
msgstr "gdb-mi: no s'ha pogut analitzar la darrera resposta: %1"

#: debugview_dap.cpp:169
#, kde-format
msgid "DAP backend failed"
msgstr "Ha fallat el dorsal DAP"

#: debugview_dap.cpp:211
#, kde-format
msgid "program terminated"
msgstr "programa terminat"

#: debugview_dap.cpp:223
#, kde-format
msgid "requesting disconnection"
msgstr "se sol·licita la desconnexió"

#: debugview_dap.cpp:237
#, kde-format
msgid "requesting shutdown"
msgstr "se sol·licita l'aturada"

#: debugview_dap.cpp:261
#, kde-format
msgid "DAP backend: %1"
msgstr "Dorsal DAP: %1"

#: debugview_dap.cpp:285
#, kde-format
msgid "Breakpoint(s) reached:"
msgstr "S'ha arribat al/s punt/s d'interrupció:"

#: debugview_dap.cpp:307
#, kde-format
msgid "(continued) thread %1"
msgstr "(ha continuat) fil %1"

#: debugview_dap.cpp:309
#, kde-format
msgid "all threads continued"
msgstr "tots els fils han continuat"

#: debugview_dap.cpp:316
#, kde-format
msgid "(running)"
msgstr "(en execució)"

#: debugview_dap.cpp:404
#, kde-format
msgid "*** connection with server closed ***"
msgstr "*** s'ha tancat la connexió amb el servidor ***"

#: debugview_dap.cpp:411
#, kde-format
msgid "program exited with code %1"
msgstr "el programa ha finalitzat amb el codi %1"

#: debugview_dap.cpp:425
#, kde-format
msgid "*** waiting for user actions ***"
msgstr "*** en espera d'accions d'usuari ***"

#: debugview_dap.cpp:430
#, kde-format
msgid "error on response: %1"
msgstr "error en la resposta: %1"

#: debugview_dap.cpp:445
#, kde-format
msgid "important"
msgstr "important"

#: debugview_dap.cpp:448
#, kde-format
msgid "telemetry"
msgstr "telemetria"

#: debugview_dap.cpp:467
#, kde-format
msgid "debugging process [%1] %2"
msgstr "depuració del procés [%1] %2"

#: debugview_dap.cpp:469
#, kde-format
msgid "debugging process %1"
msgstr "depuració del procés %1"

#: debugview_dap.cpp:472
#, kde-format
msgid "Start method: %1"
msgstr "Mètode d'inici: %1"

#: debugview_dap.cpp:479
#, kde-format
msgid "thread %1"
msgstr "fil %1"

#: debugview_dap.cpp:633
#, kde-format
msgid "breakpoint set"
msgstr "s'ha establert un punt d'interrupció"

#: debugview_dap.cpp:641
#, kde-format
msgid "breakpoint cleared"
msgstr "s'ha netejat un punt d'interrupció"

#: debugview_dap.cpp:700
#, kde-format
msgid "(%1) breakpoint"
msgstr "(%1) punt d'interrupció"

#: debugview_dap.cpp:717
#, kde-format
msgid "<not evaluated>"
msgstr "<no avaluat>"

#: debugview_dap.cpp:739
#, kde-format
msgid "server capabilities"
msgstr "capacitats del servidor"

#: debugview_dap.cpp:742
#, kde-format
msgid "supported"
msgstr "implementat"

#: debugview_dap.cpp:742
#, kde-format
msgid "unsupported"
msgstr "no implementat"

#: debugview_dap.cpp:745
#, kde-format
msgid "conditional breakpoints"
msgstr "punts d'interrupció condicionals"

#: debugview_dap.cpp:746
#, kde-format
msgid "function breakpoints"
msgstr "punts d'interrupció de funció"

#: debugview_dap.cpp:747
#, kde-format
msgid "hit conditional breakpoints"
msgstr "assoleix els punts d'interrupció condicionals"

#: debugview_dap.cpp:748
#, kde-format
msgid "log points"
msgstr "punts de registre"

#: debugview_dap.cpp:748
#, kde-format
msgid "modules request"
msgstr "sol·licitud de mòdul"

#: debugview_dap.cpp:749
#, kde-format
msgid "goto targets request"
msgstr "sol·licitud d'anar a objectius"

#: debugview_dap.cpp:750
#, kde-format
msgid "terminate request"
msgstr "sol·licitud de terminació"

#: debugview_dap.cpp:751
#, kde-format
msgid "terminate debuggee"
msgstr "depuració de terminació"

#: debugview_dap.cpp:958
#, kde-format
msgid "syntax error: expression not found"
msgstr "error de sintaxi: no s'ha trobat l'expressió"

#: debugview_dap.cpp:976 debugview_dap.cpp:1011 debugview_dap.cpp:1049
#: debugview_dap.cpp:1083 debugview_dap.cpp:1119 debugview_dap.cpp:1155
#: debugview_dap.cpp:1191 debugview_dap.cpp:1291 debugview_dap.cpp:1353
#, kde-format
msgid "syntax error: %1"
msgstr "error de sintaxi: %1"

#: debugview_dap.cpp:984 debugview_dap.cpp:1019 debugview_dap.cpp:1298
#: debugview_dap.cpp:1361
#, kde-format
msgid "invalid line: %1"
msgstr "línia no vàlida: %1"

#: debugview_dap.cpp:991 debugview_dap.cpp:996 debugview_dap.cpp:1026
#: debugview_dap.cpp:1031 debugview_dap.cpp:1322 debugview_dap.cpp:1327
#: debugview_dap.cpp:1368 debugview_dap.cpp:1373
#, kde-format
msgid "file not specified: %1"
msgstr "fitxer no especificat: %1"

#: debugview_dap.cpp:1061 debugview_dap.cpp:1095 debugview_dap.cpp:1131
#: debugview_dap.cpp:1167 debugview_dap.cpp:1203
#, kde-format
msgid "invalid thread id: %1"
msgstr "ID de fil no vàlid: %1"

#: debugview_dap.cpp:1067 debugview_dap.cpp:1101 debugview_dap.cpp:1137
#: debugview_dap.cpp:1173 debugview_dap.cpp:1209
#, kde-format
msgid "thread id not specified: %1"
msgstr "ID de fil no especificat: %1"

#: debugview_dap.cpp:1220
#, kde-format
msgid "Available commands:"
msgstr "Ordres disponibles:"

#: debugview_dap.cpp:1308
#, kde-format
msgid "conditional breakpoints are not supported by the server"
msgstr ""
"els punts d'interrupció condicionals no estan implementats en el servidor"

#: debugview_dap.cpp:1316
#, kde-format
msgid "hit conditional breakpoints are not supported by the server"
msgstr ""
"assoleix els punts d'interrupció condicionals no està implementat en el "
"servidor"

#: debugview_dap.cpp:1336
#, kde-format
msgid "line %1 already has a breakpoint"
msgstr "la línia %1 ja té un punt d'interrupció"

#: debugview_dap.cpp:1381
#, kde-format
msgid "breakpoint not found (%1:%2)"
msgstr "no s'ha trobat el punt d'interrupció (%1:%2)"

#: debugview_dap.cpp:1387
#, kde-format
msgid "Current thread: "
msgstr "Fil actual: "

#: debugview_dap.cpp:1392 debugview_dap.cpp:1399 debugview_dap.cpp:1423
#, kde-format
msgid "none"
msgstr "cap"

#: debugview_dap.cpp:1395
#, kde-format
msgid "Current frame: "
msgstr "Marc actual: "

#: debugview_dap.cpp:1402
#, kde-format
msgid "Session state: "
msgstr "Estat de la sessió: "

#: debugview_dap.cpp:1405
#, kde-format
msgid "initializing"
msgstr "s'està inicialitzant"

#: debugview_dap.cpp:1408
#, kde-format
msgid "running"
msgstr "en execució"

#: debugview_dap.cpp:1411
#, kde-format
msgid "stopped"
msgstr "aturada"

#: debugview_dap.cpp:1414
#, kde-format
msgid "terminated"
msgstr "terminada"

#: debugview_dap.cpp:1417
#, kde-format
msgid "disconnected"
msgstr "desconnectada"

#: debugview_dap.cpp:1420
#, kde-format
msgid "post mortem"
msgstr "post mortem"

#: debugview_dap.cpp:1476
#, kde-format
msgid "command not found"
msgstr "no s'ha trobat l'ordre"

#: debugview_dap.cpp:1497
#, kde-format
msgid "missing thread id"
msgstr "manca l'ID de fil"

#: debugview_dap.cpp:1605
#, kde-format
msgid "killing backend"
msgstr "s'està matant el dorsal"

#: debugview_dap.cpp:1663
#, kde-format
msgid "Current frame [%3]: %1:%2 (%4)"
msgstr "Marc actual [%3]: %1:%2 (%4)"

#: localsview.cpp:17
#, kde-format
msgid "Symbol"
msgstr "Símbol"

#: localsview.cpp:18
#, kde-format
msgid "Value"
msgstr "Valor"

#: localsview.cpp:41
#, kde-format
msgid "type"
msgstr "tipus"

#: localsview.cpp:50
#, kde-format
msgid "indexed items"
msgstr "elements indexats"

#: localsview.cpp:53
#, kde-format
msgid "named items"
msgstr "elements amb nom"

#: plugin_kategdb.cpp:103
#, kde-format
msgid "Kate Debug"
msgstr "Depuració del Kate"

#: plugin_kategdb.cpp:107
#, kde-format
msgid "Debug View"
msgstr "Vista del depurador"

#: plugin_kategdb.cpp:107 plugin_kategdb.cpp:340
#, kde-format
msgid "Debug"
msgstr "Depura"

#: plugin_kategdb.cpp:110 plugin_kategdb.cpp:113
#, kde-format
msgid "Locals and Stack"
msgstr "Locals i pila"

#: plugin_kategdb.cpp:165
#, kde-format
msgctxt "Column label (frame number)"
msgid "Nr"
msgstr "Núm"

#: plugin_kategdb.cpp:165
#, kde-format
msgctxt "Column label"
msgid "Frame"
msgstr "Marc"

#: plugin_kategdb.cpp:197
#, kde-format
msgctxt "Tab label"
msgid "Debug Output"
msgstr "Sortida de depuració"

#: plugin_kategdb.cpp:198
#, kde-format
msgctxt "Tab label"
msgid "Settings"
msgstr "Configuració"

#: plugin_kategdb.cpp:240
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<title>Could not open file:</title><nl/>%1<br/>Try adding a search path to "
"Advanced Settings -> Source file search paths"
msgstr ""
"<title>No s'ha pogut obrir el fitxer:</title><nl/>%1<br/>Intenteu-ho afegint "
"un camí de cerca a Configuració avançada -> Camins de cerca de fitxers de "
"codi font"

#: plugin_kategdb.cpp:265
#, kde-format
msgid "Start Debugging"
msgstr "Inicia la depuració"

#: plugin_kategdb.cpp:275
#, kde-format
msgid "Kill / Stop Debugging"
msgstr "Mata/atura la depuració"

#: plugin_kategdb.cpp:282
#, kde-format
msgid "Continue"
msgstr "Continua"

# skip-rule: k-Break-1
#: plugin_kategdb.cpp:288
#, kde-format
msgid "Toggle Breakpoint / Break"
msgstr "Commuta entre punt d'interrupció/interrupció"

#: plugin_kategdb.cpp:294
#, kde-format
msgid "Step In"
msgstr "Avança per dins"

#: plugin_kategdb.cpp:301
#, kde-format
msgid "Step Over"
msgstr "Avança per sobre"

#: plugin_kategdb.cpp:308
#, kde-format
msgid "Step Out"
msgstr "Avança per fora"

#: plugin_kategdb.cpp:315 plugin_kategdb.cpp:347
#, kde-format
msgid "Run To Cursor"
msgstr "Executa fins al cursor"

#: plugin_kategdb.cpp:322
#, kde-format
msgid "Restart Debugging"
msgstr "Reinicia la depuració"

#: plugin_kategdb.cpp:330 plugin_kategdb.cpp:349
#, kde-format
msgctxt "Move Program Counter (next execution)"
msgid "Move PC"
msgstr "Mou el CP"

#: plugin_kategdb.cpp:335
#, kde-format
msgid "Print Value"
msgstr "Imprimeix el valor"

#: plugin_kategdb.cpp:344
#, kde-format
msgid "popup_breakpoint"
msgstr "popup_breakpoint"

#: plugin_kategdb.cpp:346
#, kde-format
msgid "popup_run_to_cursor"
msgstr "popup_run_to_cursor"

#: plugin_kategdb.cpp:428 plugin_kategdb.cpp:444
#, kde-format
msgid "Insert breakpoint"
msgstr "Insereix un punt d'interrupció"

#: plugin_kategdb.cpp:442
#, kde-format
msgid "Remove breakpoint"
msgstr "Elimina el punt d'interrupció"

#: plugin_kategdb.cpp:571
#, kde-format
msgid "Execution point"
msgstr "Punt d'execució"

#: plugin_kategdb.cpp:710
#, kde-format
msgid "Thread %1"
msgstr "Fil %1"

#: plugin_kategdb.cpp:810
#, kde-format
msgid "IO"
msgstr "E/S"

#: plugin_kategdb.cpp:894
#, kde-format
msgid "Breakpoint"
msgstr "Punt d'interrupció"

#. i18n: ectx: Menu (debug)
#: ui.rc:6
#, kde-format
msgid "&Debug"
msgstr "&Depura"

#. i18n: ectx: ToolBar (gdbplugin)
#: ui.rc:29
#, kde-format
msgid "Debug Plugin"
msgstr "Connector de depuració"
